# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
from bson import json_util 
import json 
# Instantiate the app

"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, Response
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient


import logging

###
# Globals
###
app = flask.Flask(__name__)

api = Api(app)

CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###
client = MongoClient('mongodb://mongodb:27017/')



'''
#uncomment for testing locally
#client = MongoClient(host="0.0.0.0",port= 8000)

'''

db = client.calcdb




@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    

    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    start_date = request.args.get('start_date', "fail", type=str)
    start_time = request.args.get('start_time', "fail", type=str)
    brevet_dist_km = request.args.get('brevet_dist_km', "fail", type=float)

    app.logger.debug("km={}".format(km))
    app.logger.debug("start_time={}".format(start_time))
    app.logger.debug("start_date={}".format(start_date))
    app.logger.debug("brevet_distance={}".format(brevet_dist_km))
    app.logger.debug("request.args: {}".format(request.args))


    time_object = arrow.get(start_date + " " +start_time, 'YYYY-MM-DD HH:mm')
    #app.logger.debug("the object" , time_object)
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, time_object)
    close_time = acp_times.close_time(km, brevet_dist_km, time_object)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/new', methods=['POST'])
def new():

    """
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)
    """
    print("got to post place")
    db.calcdb.delete_many({})

    data = request.form

    close_list = request.form.getlist("close_times[]", type=str)
    open_list = request.form.getlist("open_times[]", type=str)
    km_list = request.form.getlist("the_kms[]", type=str)
    miles_list = request.form.getlist("the_miles[]", type=str)

    #print(open_list)
    #print(close_list)
    #print(km_list)
    #print(miles_list)
    keep_going = True
    counter = 0

    while keep_going:
        if km_list[counter] == "":
            break
        item_doc = {
            'control': "Control "+str(counter+1),
            'km': "km: " +str(km_list[counter]),
            'mile': "mile: " +str(miles_list[counter]),
            'open': "Open Time: " +str(open_list[counter]),
            'close': "Close Time: " +str(close_list[counter])

        }

        
        db.calcdb.insert_one(item_doc)

        if counter == 19:
            keep_going = False
            break

        counter += 1




    response = Response(status=200)

    return response

@app.route("/display")
def _display_db():
    _items = db.calcdb.find()
    items = [item for item in _items]
    print(items)

    db.calcdb.delete_many({})

    return flask.render_template('show_db.html',items=items)



## JSONS
#####################################################################

@app.route("/listAll/json")
@app.route("/listAll")
def get1():


	_items = db.calcdb.find()


	items = [item for item in _items]

	the_opens = []

	for item in items:
		the_opens.append(item['open'])


	
	_items = db.calcdb.find()


	items = [item for item in _items]

	the_closes = []
	
	for item in items:
		the_closes.append(item['close'])
		

	return {'Opens': the_opens, 'Closes': the_closes}
#####################################################################

#####################################################################

@app.route("/listOpenOnly/json")
@app.route("/listOpenOnly")
def get2():

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_opens = []
		
			for item in items:
				the_opens.append(item['open'])
			

			return {'Opens': the_opens}
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_opens = []
	
		for item in items:
			the_opens.append(item['open'])
		

		return {'Opens': the_opens}

#############################################################

@app.route("/listCloseOnly/json")
@app.route("/listCloseOnly")
def get3():

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_closes = []
		
			for item in items:
				the_closes.append(item['close'])
			

			return {'Closes': the_closes}
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_closes = []
	
		for item in items:
			the_closes.append(item['close'])
		

		return {'Closes': the_closes}


############################################################

@app.route("/listAll/csv")
def get4():
	_items = db.calcdb.find()


	items = [item for item in _items]

	the_opens = []
	the_closes = []

	for item in items:
		the_opens.append(item['open'])

	for item in items:
		the_closes.append(item['close'])

	csv = "Open Time,"
	csv += "Close Time\n"

	stop_point_newline = len(the_closes) - 1
	
	for i in range(len(the_closes)):
		if i != stop_point_newline:
			csv += (the_opens[i] + "," + the_closes[i] + "\n")
		else:
			csv += (the_opens[i] + "," + the_closes[i])
	
	print(csv)


	return csv

############################################################

@app.route("/listOpenOnly/csv")
def get5():
	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_opens = []
		
			for item in items:
				the_opens.append(item['open'])
			

			csv = "\n".join(the_opens)

			csv = "Open Time\n" + csv

			return csv
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_opens = []
	
		for item in items:
			the_opens.append(item['open'])

		csv = "\n".join(the_opens)
		print(csv)

		csv = "Open Time\n" + csv
		

		return csv

############################################################

@app.route("/listCloseOnly/csv")
def get6():
	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_closes = []
		
			for item in items:
				the_closes.append(item['close'])
			

			csv = "\n".join(the_closes)

			csv = "Close Time\n" + csv

			return csv
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_closes = []
	
		for item in items:
			the_closes.append(item['close'])

		csv = "\n".join(the_closes)
		print(csv)

		csv = "Close Time\n" + csv
		

		return csv

############################################################

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
       

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/test')



"""

class the_json(Resource):
    def get(self):
    	#my_doc = db.find_one('_id')
    	_items = db.calcdb.find()


    	items = [item for item in _items]

    	the_opens = []
    	
    	for item in items:
    		print("hello!!!!", item)
    		the_opens.append(item['open'])
    		
 
    	return {
            'Opens': the_opens
        }


api.add_resource(the_json, '/listOpenOnly')
"""




# Run the application

#app.debug = CONFIG.DEBUG
#if app.debug:
    #app.logger.setLevel(logging.DEBUG)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)

